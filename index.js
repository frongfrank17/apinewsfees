const express = require('express')
const app = express()
const bodyParser = require('body-parser')
const https = require('https')
const axios = require('axios')

// db mongo
var MongoClient = require('mongodb').MongoClient;
var url = "mongodb://localhost:27017/";

//----
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))

app.get('/news', (req, res) => {

    MongoClient.connect(url, function(err, db) {
      if (err) throw err;
      var dbo = db.db("db_spunews");
      dbo.collection("tb_news").find({}).toArray(function(err, result) {
        if (err) throw err;
          res.json(result)
      });
    });
})
app.get('/inesrtNews',(req,res) =>{
  MongoClient.connect(url, function(err, db) {
    if (err) throw err;
    var dbo = db.db("db_spunews");
    var myobj = { 
      topic:req.query.topic,
      detail:req.query.detail,
      write:req.query.write,
      department:req.query.department,
    };
    dbo.collection("tb_news").insertOne(myobj, function(err, res) {
      if (err) throw err;
      console.log("1 document inserted");
     // console.log("data :"+JSON.stringify(myobj))
      db.close();
    });
  });
})
//profile
app.get('/login',(req,res)=>{
  var login = {
    username:req.query.username,
    password:req.query.password
  }
  MongoClient.connect(url,{useNewUrlParser:true}, function(err, db) {
    if (err) throw err;
    var dbo = db.db("db_spunews");
    dbo.collection("tb_user").find(login).toArray(function(err, result) {
      if (err) throw err;
        res.json(result)
    });
  });
})
app.get( '/getuser',(req,res)=>{
  var login ={
    username: req.query.username
  }
  MongoClient.connect(url, {useNewUrlParser:true},function(err, db) {
    if (err) throw err;
    var dbo = db.db("db_spunews");
    dbo.collection("tb_user").find(login).toArray(function(err, result) {
      if (err) throw err;
        res.json(result)
    });
  }); 
});

app.get('/register',(req,res) =>{
  
  MongoClient.connect(url, function(err, db) {
    if (err) throw err;
    var dbo = db.db("db_spunews");
    var myobj = { 
      username: req.query.username,
      name:req.query.name,
      password:req.query.password,
      department:req.query.department
    };
    dbo.collection("tb_user").insertOne(myobj, function(err, res) {
      if (err) throw err;
      console.log("1 document inserted");
      db.close();
    });
  });
})
app.get('/Editprofile', (req,res) => {
  var query = { username:req.query.username};
  var newvalues = {
     $set: {name:req.query.name,
            department:req.query.department
            } };
  MongoClient.connect(url, function(err, db) {
    if (err) throw err;
    var dbo = db.db("db_spunews");
    dbo.collection("tb_user").updateOne(query,newvalues,function (err,res){
      if(err) throw err;
      console.log("update")
      db.close();
    })
  });
})
app.get('/Editpassword', (req,res) => {
  var query = { username:req.query.username};
  var newvalues = {
     $set: {password:req.query.password,
            } };
  MongoClient.connect(url, function(err, db) {
    if (err) throw err;
    var dbo = db.db("db_spunews");
    dbo.collection("tb_user").updateOne(query,newvalues,function (err,res){
      if(err) throw err;
      console.log("update")
      db.close();
    })
  });
})


app.listen(8081, () => {
  console.log('Start server at port 8081')
})
